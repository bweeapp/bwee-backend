class Api::Auth::SessionsController < DeviseTokenAuth::SessionsController
  include Api::BaseController::JsonRequestsForgeryBypass
end
