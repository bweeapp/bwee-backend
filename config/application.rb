require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Dotenv::Railtie.load

module BweeBackend
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # allow all origins
    config.middleware.insert_before 0, Rack::Cors, debug: true, logger: (-> { Rails.logger }) do
      allow do
        origins '*'
        resource "*",
          headers: :any,
          expose: [
            "access-token",
            "expiry",
            "token-type",
            "uid",
            "client"
          ],
          methods: [
            :get,
            :put,
            :post,
            :head,
            :patch,
            :delete,
            :options
          ]
      end
    end
  end
end
